import csv
import re


def write_csv(searchNumber, url, text, title):
    textNew = re.sub("^\s+|\n|\s+$", '', text)
    data = [url, title, textNew]
    with open(f'news/{searchNumber}.csv', 'a', encoding='utf-8') as f:
        writer = csv.writer(f)
        writer.writerow(data)


def get_data_in_csv(searchNumber: str, words: list, flagWords: bool):
    data = []
    with open(f'news/{searchNumber}.csv', 'r', encoding='utf-8') as f:
        reader = csv.reader(f)
        for row in reader:
            if row:
                for i in words:
                    if i in row[1] and flagWords:
                        data.append({'url': row[0], 'title': row[1], 'text': row[2]})

                    elif not flagWords:
                        data.append({'url': row[0], 'title': row[1], 'text': row[2]})
            else:
                continue
    return data

# def writeAll(url, mess, chanel, *args):
#     # from_url = re.search("(?P<url>https?://[^\s]+)", mess).group("url")#ссылки из тела сообщениия
#     if not mess:
#         data = [url, mess]
#         for ar in args:
#             data.append(ar)
#
#         with open(f'dataset_csv/{chanel}.csv', 'a') as f:
#             writer = csv.writer(f)
#             writer.writerow(data)
#
#     else:
#         text = re.sub(r'^https?:\/\/.*[\r\n]*', '', mess, flags=re.MULTILINE)
#         mess_ = re.sub(r'\W+', ' ', text)
#
#         data = [url, mess_]
#         for ar in args:
#             data.append(ar)
#
#         with open(f'dataset_csv/{chanel}.csv', 'a') as f:
#             writer = csv.writer(f)
#             writer.writerow(data)
